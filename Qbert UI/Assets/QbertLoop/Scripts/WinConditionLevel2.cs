﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- Will detect if all the cubes in the array are a specific color. If so, the player wins.  This only applies to Level 1.
//   Start & End dates- October 30th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinConditionLevel2 : MonoBehaviour {

	[SerializeField] GameObject[] cubes;
	int coloredCube;
	public bool allTurned = false;

	public GameObject PlayerCharacter;

	public AudioClip victory;
	public AudioSource AS;
	public float musicTimer;
	public float musicTimerLimit = 1.0f;

	// Use this for initialization
	void Start () {

	}

	void Awake(){
		cubes = GameObject.FindGameObjectsWithTag ("Cube");
		musicTimer = musicTimerLimit;
	}

	// Update is called once per frame
	void Update () {
		coloredCube = 0;
		for(int i = 0; i < cubes.Length; i ++){
			if(cubes[i].gameObject.GetComponent <Renderer>().material.color == Color.green){
				coloredCube++;
			}
			if(coloredCube == cubes.Length){
				Debug.Log ("You win!");
				allTurned = true;
				PlayerCharacter.GetComponent <JR_Movement>().enabled = false;
				StartCoroutine (MusicPlay ());
			}
		}
	}

	IEnumerator MusicPlay(){
		musicTimer -= Time.deltaTime;
		if(musicTimer > 0.0f){
			AS.clip = victory;
			AS.PlayOneShot (victory);
		}
		else{
			musicTimer = (0);
		}
		Debug.Log ("victory music");
		yield return new WaitForSeconds (1.0f);
		StopCoroutine (MusicPlay ());
	}
}

