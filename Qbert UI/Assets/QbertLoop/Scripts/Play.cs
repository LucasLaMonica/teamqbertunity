﻿/*Anthony DiMarco
 * 
 Contribution: Created code for quitting and starting game.
 Feature: Main Menu
 Started: 11/1/17 9:40 pm
 ended: 11/1/17 9:57 pm

*/
/*
Partially writen by Johnathon Hrifko
Setup inital starting lives for the game. (line 35)
	Start and End (11/9/18)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Play : MonoBehaviour {


    public void NewGameBtn(string NewGame)
    {
        SceneManager.LoadScene(NewGame);
    }
    
    public void end()
    {
        Application.Quit();
    }

    // Use this for initialization
    void Awake () {

		PlayerPrefs.SetInt("Lives",6);
        PlayerPrefs.SetInt("FinalScore", 0);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
