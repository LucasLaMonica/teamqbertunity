﻿/*Anthony DiMarco
 * 
 Contribution: Adding code to allow the player to return to the Main Menu.
 Feature: UI
 Started: 11/7/17 10:10 am
 ended: 11/1/17 10:12 am*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturntoMenu : MonoBehaviour {

    public void Return(string Menu)
    {
        SceneManager.LoadScene(Menu);
    }

}
