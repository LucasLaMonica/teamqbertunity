﻿/*
Writen by Johnathon Hrifko
Setup for the main menu items for being displayed.  This is not being used at the moment but, is trying to be added for the final build.
Start and End (11/9/18)
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoard : MonoBehaviour {
	public Text LeaderName1;
	public Text LeaderName2;
	public Text LeaderName3;
	public Text LeaderName4;
	public Text LeaderName5;
	public Text LeaderName6;
	public Text LeaderName7;
	public Text LeaderName8;
	public Text LeaderName9;
	public Text LeaderName10;
	public int Leader1;
	public int Leader2;
	public int Leader3;
	public int Leader4;
	public int Leader5;
	public int Leader6;
	public int Leader7;
	public int Leader8;
	public int Leader9;
	public int Leader10;
	public int YourScore;

	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {

		Leader1 = PlayerPrefs.GetInt("Leader1");
		Leader2 = PlayerPrefs.GetInt("Leader2");
		Leader3 = PlayerPrefs.GetInt("Leader3");
		Leader4 = PlayerPrefs.GetInt("Leader4");
		Leader5 = PlayerPrefs.GetInt("Leader5");
		Leader6 = PlayerPrefs.GetInt("Leader6");
		Leader7 = PlayerPrefs.GetInt("Leader7");
		Leader8 = PlayerPrefs.GetInt("Leader8");
		Leader9 = PlayerPrefs.GetInt("Leader9");
		Leader10 = PlayerPrefs.GetInt("Leader10");
		YourScore = PlayerPrefs.GetInt("FinalScore");

	}
}
