﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- In the color-changing tests, this test character will perform simple movements on button imputs.
//   Start & End dates- October 26th
//   References:
//   Links: 
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	private Rigidbody RB;

	public bool disableInput;
	public float speed;
	public float rotateSpeed;

	public float JumpSpeed = 3.0f;
	private bool IsGrounded;
	public float JumpRate = 1.0f;
	float NextJump = 0.0f;

	// Use this for initialization
	void Start () {
		RB = gameObject.GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey(KeyCode.W))
		{
			RB.position += (gameObject.transform.forward * speed * Time.deltaTime);
		}

		if (Input.GetKey(KeyCode.S))
		{
			RB.position += (gameObject.transform.forward * speed * Time.deltaTime * -1);
		}

		if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime * -1);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKeyDown (KeyCode.Space) && Time.time > NextJump){
			NextJump = Time.time + JumpRate;
			RB.velocity = new Vector2 (RB.velocity.x, 5.0f);
		}
	}
}
