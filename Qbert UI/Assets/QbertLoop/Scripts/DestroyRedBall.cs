﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- When the red ball falls into the trigger, the specific red ball AI will be destroyed.
//   Start & End dates- November 4th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRedBall : MonoBehaviour {

	public GameObject enemyObjects;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "RedBall"){
			Debug.Log ("Fallen off the stage.");
			Destroy (enemyObjects);
		}
	}
}
