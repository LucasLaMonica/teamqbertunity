﻿/*       
// Developer Name: Jesse Rivera and Lucas LaMonica
//   Contribution:
//   Feature- The red ball will bounce downward in a specific pattern.
//   Start & End dates- October 30th- November 15th
//   References: -Unity3D- Can you unfreeze a rigidbody.constaint position as you can freeze them?
//   Links: -https://answers.unity.com/questions/23887/can-you-unfreeze-a-rigidbodyconstraint-position-as.html
//
*/
// Contribution Jesse Rivera: Start(11/4/18) End(11/9/18) Starting the script by adding a respawn, and made waypoints for the ball to follow.
// 
/*
Partially writen by Johnathon Hrifko
Setup the void on trigger enter for the lives and all the void GameOveDeath stuff which is mostly leaderboard setup which is not currently in use.
	Start and End (11/9/18)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeCollision : MonoBehaviour {
    public GameObject GameOverDeathCanvas;
    private int LifeisBad;
    public Transform respawnPosition;

    GameObject player;

	public AudioClip hitPlayer;
	public AudioSource AS;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {

            LifeisBad = PlayerPrefs.GetInt("Lives");
            LifeisBad--;
            PlayerPrefs.SetInt("Lives", LifeisBad);

            if (PlayerPrefs.GetInt("Lives") == 0)
            {
                //sends you to void GameOveDeath when you have 0 lives left.
                GameOverDeath();

            }

            else
            {
                //if you have more than 0 lives left then reset the player position.
				StartCoroutine (AfterCollision ());

            }

        }

    }


	IEnumerator AfterCollision(){
		AS.clip = hitPlayer;
		AS.Play ();
		player.GetComponent <JR_Movement> ().enabled = false;
		gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		yield return new WaitForSeconds (0.5f);
		player.transform.position = respawnPosition.transform.position;
		player.GetComponent <JR_Movement> ().enabled = true;
		gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.None;
		StopCoroutine (AfterCollision ());
	}

    void GameOverDeath()
    {
        Time.timeScale = 0;
        //turns on the Game Over canvas when GameOverDeath(); is called.
        GameOverDeathCanvas.SetActive(true);
        //start of leader Board stuff
        if (PlayerPrefs.GetInt("Leader1") < PlayerPrefs.GetInt("FinalScore"))
        {

            PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
            if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
            {

                PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

            }

            if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
            {

                PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

            }

            if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
            {

                PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

            }

            if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
            {

                PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

            }

            if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5"))
            {

                PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

            }

            if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4"))
            {

                PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

            }

            if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3"))
            {

                PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

            }

            if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2"))
            {

                PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("Leader1"));

            }

            if (PlayerPrefs.GetInt("Leader2") == PlayerPrefs.GetInt("Leader1"))
            {

                PlayerPrefs.SetInt("Leader1", PlayerPrefs.GetInt("FinalScore"));

            }
        }

        if (PlayerPrefs.GetInt("Leader2") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader1"))
        {

            PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
            if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
            {

                PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

            }

            if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
            {

                PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

            }

            if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
            {

                PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

            }

            if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
            {

                PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

            }

            if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5"))
            {

                PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

            }

            if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4"))
            {

                PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

            }

            if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3"))
            {

                PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

            }

            if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2"))
            {

                PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("FinalScore"));

            }

            if (PlayerPrefs.GetInt("Leader3") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader2"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

                }

                if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
                {

                    PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

                }

                if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
                {

                    PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

                }

                if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5"))
                {

                    PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

                }

                if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4"))
                {

                    PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

                }

                if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3"))
                {

                    PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("FinalScore"));

                }

            }
            if (PlayerPrefs.GetInt("Leader4") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader3"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

                }

                if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
                {

                    PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

                }

                if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
                {

                    PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

                }

                if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5"))
                {

                    PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

                }

                if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4"))
                {

                    PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader5") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader4"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

                }

                if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
                {

                    PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

                }

                if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
                {

                    PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

                }

                if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5"))
                {

                    PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader6") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader5"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

                }

                if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
                {

                    PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

                }

                if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6"))
                {

                    PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader7") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader6"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

                }

                if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7"))
                {

                    PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader8") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader7"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

                }

                if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8"))
                {

                    PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader9") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader8"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
                if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9"))
                {

                    PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("FinalScore"));

                }

            }

            if (PlayerPrefs.GetInt("Leader10") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader9"))
            {

                PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("FinalScore"));

            }
            //End of Leader Board Stuff}
        }
    }

}
