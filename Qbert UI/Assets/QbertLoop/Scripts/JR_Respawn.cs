﻿// Contribution Jesse Rivera: Start(11/4/18) End(11/5/18) Made the basic respawn of the player and enemy
/*
Partially writen by Johnathon Hrifko
Setup for leaderboards which are not currently in use.
	Start and End (11/9/18)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JR_Respawn : MonoBehaviour
{
    public Transform player;
    public Transform respawnPosition;
    public GameObject GameOverDeathCanvas;
    public GameObject LivesLeftCounterCanvas;
    public Text LivesLeftText;
    private int Livesleft;

    void Start()
    {
        
        //Sets the Game Over canvas to off in case it is on when game starts.
        GameOverDeathCanvas.SetActive(false);
        //Sets The Lives Counter Canvas to active in case of it being turned off in the editor.
        LivesLeftCounterCanvas.SetActive(true);
		//sets the livesleft to the actual lives left.
		Livesleft = PlayerPrefs.GetInt("Lives");
    }
    void Update()
    {
        //setting the text for amount of lives left.
		LivesLeftText.text = "Lives Left: " + PlayerPrefs.GetInt("Lives");
		if (PlayerPrefs.GetInt("Lives") <= 0){

			GameOverDeath();

		}

    }

    void OnTriggerEnter(Collider other)
    {
        //decreasing lives by 1 when the trigger is hit.
        player.transform.position = respawnPosition.transform.position;
        Livesleft--;
        Debug.Log("Lost 1 life");
		PlayerPrefs.SetInt("Lives",Livesleft);

        if (PlayerPrefs.GetInt("Lives") == 0)
        {
            //sends you to void GameOveDeath when you have 0 lives left.
            GameOverDeath();

        }

        /*else
        {
            //if you have more than 0 lives left then reset the player position.
            player.transform.position = respawnPosition.transform.position;

        }*/

    }
    void GameOverDeath()
    {
        Time.timeScale = 0;
        //turns on the Game Over canvas when GameOverDeath(); is called.
        GameOverDeathCanvas.SetActive(true);
		//start of leader Board stuff
		if (PlayerPrefs.GetInt("Leader1") < PlayerPrefs.GetInt("FinalScore")){

			PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
			if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

			}

			if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

			}

			if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

			}

			if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

			}

			if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

			}

			if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

			}

			if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

			}

			if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("Leader1"));

			}

			if (PlayerPrefs.GetInt("Leader2") == PlayerPrefs.GetInt("Leader1")){

				PlayerPrefs.SetInt("Leader1", PlayerPrefs.GetInt("FinalScore"));

			}
		}

		if (PlayerPrefs.GetInt("Leader2") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader1")){

			PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
			if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

			}

			if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

			}

			if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

			}

			if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

			}

			if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

			}

			if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

			}

			if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

			}

			if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("FinalScore"));

			}

			if (PlayerPrefs.GetInt("Leader3") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

				}

				if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

					PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

				}

				if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

					PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("FinalScore"));

				}

			}
			if (PlayerPrefs.GetInt("Leader4") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

				}

				if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

					PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader5") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader6") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader7") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader8") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader9") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader10") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("FinalScore"));

			}
			//End of Leader Board Stuff

    }
}
}

