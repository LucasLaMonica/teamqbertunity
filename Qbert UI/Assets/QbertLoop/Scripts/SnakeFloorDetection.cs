﻿//Contribution Jesse Rivera: Start(11/9/18) End(11/9/18) Created a script where the snake detects if the floor is available or not available.
//Contribution Lucas LaMonica Start(11/15/18) End(11/15/18) Make sure a sound effect would play whenever the player moves.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeFloorDetection : MonoBehaviour {
	public string message;

	public AudioClip jumpLand;
	public AudioSource AS;

	void OnTriggerEnter(Collider other){
		AS.clip = jumpLand;
		AS.Play ();
	}

	void OnTriggerStay(Collider other){
		transform.parent.SendMessage (message);
	}
	void OnTriggerExit(Collider other){
		transform.parent.SendMessage ("not"+message);
	}
}
