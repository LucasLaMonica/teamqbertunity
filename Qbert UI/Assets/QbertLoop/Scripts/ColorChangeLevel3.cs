﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- When QBert touches an cube, the cube's color will altercate back and forth between two colors. This will only work in Level 3.
//   Start & End dates- October 30th-November 15th
//   References: Unity 3D- Detect a gameObject's tag via OnCollisionEnter()
//   Links: https://answers.unity.com/questions/154433/detect-a-gameobjects-tag-via-oncollisionenter.html
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorChangeLevel3 : MonoBehaviour {

    public int scoreValue;
    public ScoreStuff gameController;

    public bool isCyan = true;
	public bool isBlue = false;

	public float colorSpeed = 0.2f;
	public float Timer;
	public float TimerLimit = 2.0f;

	public WinConditionLevel1 win;

	public string LoadLevel;

	public GameObject Player;

	public bool topBlock;

	// Use this for initialization
	void Start () { 
        // !!START OF NEW STUFF!! Sets up gameController (on WinCondition) and allows it to be used
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<ScoreStuff>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        // !! END OF NEW STUFF!!

		gameObject.GetComponent <Renderer>().material.color = Color.cyan;
		Timer = TimerLimit;
	}

	void OnCollisionEnter(Collision other){
		if(other.collider.tag == "Player" || other.collider.tag == "Slick"){
			Debug.Log ("Collision");
			if(gameObject.GetComponent <Renderer>().material.color == Color.cyan && isCyan == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.blue;
				StartCoroutine (WaitForChangeCyan());

				// !! START OF NEW STUFF!! Adds points when the player changes the color of a cube.
				gameController.AddScore(scoreValue);
				Debug.Log("25 Points added!");
				// !! END OF NEW STUFF
			}
		}

		if (other.collider.tag == "Player" || other.collider.tag == "Slick"){
			if(gameObject.GetComponent <Renderer>().material.color == Color.blue && isBlue == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.cyan;
				StartCoroutine (WaitForChangeBlue ());
			}
		}
	}

	void OnCollisionExit(Collision other){
		if(other.collider.tag == "Player" && topBlock == true){
			topBlock = false;
		}
	}

	// Update is called once per frame
	void Update () {
		if(win.allTurned == true){
			StartCoroutine (VictoryFlash());
		}
	}

	IEnumerator WaitForChangeCyan(){
		yield return new WaitForSeconds(0.5f);
		isCyan = false;
		isBlue = true;
		StopCoroutine (WaitForChangeCyan ());
	}

	IEnumerator WaitForChangeBlue(){
		yield return new WaitForSeconds(0.5f);
		isCyan = true;
		isBlue = false;
		StopCoroutine (WaitForChangeBlue ());
	}

	IEnumerator VictoryFlash(){
		Timer -= Time.deltaTime;
		if(Timer > 0.0f){
			if(gameObject.GetComponent <Renderer>().material.color == Color.blue){
				yield return new WaitForSeconds (0.15f);
				gameObject.GetComponent <Renderer>().material.color = Color.cyan; 
			}
			if(gameObject.GetComponent <Renderer>().material.color == Color.cyan){
				yield return new WaitForSeconds (0.15f);
				gameObject.GetComponent <Renderer>().material.color = Color.blue; 
			}
		}
		if(Timer <= 0.0f){
			//Timer = (0);
			Debug.Log ("Time's up");
			gameObject.GetComponent <Renderer>().material.color = Color.blue; 
			yield return new WaitForSeconds (0.3f);
			Player.SetActive (false);
			yield return new WaitForSeconds (1.3f);
			SceneManager.LoadScene (LoadLevel);
		}
	}
}