﻿//Contribution Jesse Rivera: Start(11/9/18) End(11/9/18) Created the basic snake AI that follows the player with the same movement as the player controller. Made it also that the AI doesn't go out of bounds.
/*
Partially writen by Johnathon Hrifko
Setup the void on trigger enter for the lives and all the void GameOveDeath stuff which is mostly leaderboard setup which is not currently in use.
	Start and End (11/9/18)
	*/
/*
Partially written by Lucas LaMonica
	//When the win condition is met, the snake will freeze in place.
	//Start and End (11/15/17)
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeEnemy : MonoBehaviour {

	public float movementTimer = 0;

	private float currentTime = 1;


	bool left = false;
	bool right = false;
	bool up = false;
	bool down = false;

	public Transform leftCube;
	public Transform rightCube;
	public Transform topCube;
	public Transform downCube;
	private int LifeisBad;
	public GameObject GameOverDeathCanvas;
	public Transform respawnPosition;
	private WinConditionLevel1 WC;

	GameObject player;

	// Use this for initialization
	void Start () {
		GameOverDeathCanvas = GameObject.FindGameObjectWithTag("Death");
		respawnPosition = GameObject.FindGameObjectWithTag ("Respawn").transform;
		player = FindObjectOfType<JR_Movement> ().gameObject;
		GameOverDeathCanvas.SetActive(false);
		WC = WinConditionLevel1.FindObjectOfType<WinConditionLevel1> ();
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;

		if(currentTime>=movementTimer) {
			//Move towards players
			currentTime = 0;
			Vector3 direction = Quaternion.AngleAxis(-45,Vector3.up)*(player.transform.position - this.transform.position);
			direction.Normalize ();
			Debug.Log (direction);

			if (direction.y > 0.1 && right) {
				//player is above
				transform.position += rightCube.position - downCube.position;
				return;
			}
			if (direction.x > 0.1  && down) {
				//player is on the right
				transform.position += rightCube.position - topCube.position;
				return;
			}
			if (direction.x < -0.1  && up) {
				//player is on the left
				transform.position += leftCube.position - downCube.position;
				return;
			}
			if(direction.y <-0.1 && left){
			//player is below
				transform.position += leftCube.position - topCube.position;
				return;
			}
		}

		if(WC.allTurned == true){
			gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		}
	}

	public void setUp() {
		up = true;
	}
	public void setDown() {
		down =true;
	}
	public void setRight() {
		right = true;
	}
	public void setLeft() {
		left = true;
	}

	public void notsetUp() {
		up = false;
	}
	public void notsetDown() {
		down =false;
	}
	public void notsetRight() {
		right = false;
	}
	public void notsetLeft() {
		left = false;
	}


}
