﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
Developer Name: Alanis Arriaga and Lucas LaMonica
Contribution: A moving disc platform so that when a player touches the disc it'll freeze the players rigid body and then play an animatio of the disc moving to the top of the map.
Start Date: 11/1/2017
End Date: 11/15/2017
//*/



public class Disc : MonoBehaviour {

    private Animator anim;
    public GameObject player;
    public bool Ishere = false;
    public GameObject disc;
    public int Discscore;
    public ScoreStuff Gamecontroller; 

	public AudioClip onDisc;
	public AudioSource AS;

    private void Start()
    {
        anim = disc.GetComponent<Animator>();
	
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            Gamecontroller = gameControllerObject.GetComponent<ScoreStuff>();
        }
        if (Gamecontroller == null)
        {
            Debug.Log("Cannot find 'GameController' script");
           
        }
     
    }



    private void  OnTriggerEnter(Collider other)
    {

        if(other.tag == "Player")

            

        {
			AS.clip = onDisc;
			AS.Play ();
            //Debug.Log("AAA");
            Ishere = true;

            anim.SetBool("Rise", true);

            other.GetComponent<JR_Movement>().enabled = false;
            other.transform.parent = this.transform;
            other.GetComponent<Rigidbody>().isKinematic = true;


            Gamecontroller.AddScore(Discscore);

        }
    }

    
}
