﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- Will detect if all the cubes in the array are a specific color. If so, the player wins.  This only applies to Level 3.
//   Start & End dates- October 30th
//   References:
//   Links:
//
*/
/*
Partially writen by Johnathon Hrifko
Setup for leaderboards which are not currently in use.
	Start and End (11/9/18)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinConditionLevel3 : MonoBehaviour {

	[SerializeField] GameObject[] cubes;
	int coloredCube;
	public bool allTurned = false;

	public GameObject PlayerCharacter;

	public AudioClip victory;
	public AudioSource AS;
	public float musicTimer;
	public float musicTimerLimit = 1.0f;

	// Use this for initialization
	void Start () {

	}

	void Awake(){
		cubes = GameObject.FindGameObjectsWithTag ("Cube");
		musicTimer = musicTimerLimit;
	}

	// Update is called once per frame
	void Update () {
		coloredCube = 0;
		for(int i = 0; i < cubes.Length; i ++){
			if(cubes[i].gameObject.GetComponent <Renderer>().material.color == Color.blue){
				coloredCube++;
			}
			if(coloredCube == cubes.Length){
				Debug.Log ("You win!");
				allTurned = true;
				PlayerCharacter.GetComponent <JR_Movement>().enabled = false;
				musicTimer -= Time.deltaTime;
				if(musicTimer > 0.0f){
					AS.clip = victory;
					AS.PlayOneShot (victory);
				}
				else{
					musicTimer = (0);
				}

				//start of leader Board stuff
				if (PlayerPrefs.GetInt("Leader1") < PlayerPrefs.GetInt("FinalScore")){

					PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
					if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

						PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

					}

					if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

						PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

					}

					if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

						PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

					}

					if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

						PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

					}

					if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

						PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

					}

					if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

						PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

					}

					if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

						PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

					}

					if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

						PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("Leader1"));

					}

					if (PlayerPrefs.GetInt("Leader2") == PlayerPrefs.GetInt("Leader1")){

						PlayerPrefs.SetInt("Leader1", PlayerPrefs.GetInt("FinalScore"));

					}
				}

				if (PlayerPrefs.GetInt("Leader2") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader1")){

					PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
					if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

						PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

					}

					if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

						PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

					}

					if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

						PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

					}

					if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

						PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

					}

					if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

						PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

					}

					if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

						PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

					}

					if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

						PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

					}

					if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

						PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("FinalScore"));

					}

					if (PlayerPrefs.GetInt("Leader3") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader2")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

						}

						if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

							PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

						}

						if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

							PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

						}

						if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

							PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

						}

						if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

							PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

						}

						if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

							PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("FinalScore"));

						}

				}
					if (PlayerPrefs.GetInt("Leader4") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader3")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

						}

						if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

							PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

						}

						if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

							PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

						}

						if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

							PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

						}

						if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

							PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader5") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader4")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

						}

						if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

							PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

						}

						if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

							PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

						}

						if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

							PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader6") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader5")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

						}

						if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

							PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

						}

						if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

							PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader7") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader6")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

						}

						if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

							PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader8") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader7")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

						}

						if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

							PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader9") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader8")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
						if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

							PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("FinalScore"));

						}

					}

					if (PlayerPrefs.GetInt("Leader10") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader9")){

						PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("FinalScore"));

					}
					//End of Leader Board Stuff
			}
		}
	}
}
}
