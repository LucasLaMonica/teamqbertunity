﻿/*
Writen by Johnathon Hrifko
Setup for all the buttons on the Pause Menu.
	Start and End (11/5/18)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JJH_PauseMenuButtonManager : MonoBehaviour {
    public GameObject PauseMenu;


    public void ResumeGameBtn()
    {

        PauseMenu.SetActive(false);
		Time.timeScale = 1;

    }


    public void RestartLevelBtn(string RestartLevel)
	{

		SceneManager.LoadScene(RestartLevel);

	}

	public void MainMenuBtn(string MainMenu)
	{

		SceneManager.LoadScene(MainMenu);

	}

	public void ExitGameBtn()
	{

		Application.Quit();

	}

}
