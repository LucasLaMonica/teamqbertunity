﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- After a certain increment of time, the script will cycle through an array and spawn one of the red ball AIs.
//   Start & End dates- November 4th
//   References: Unity3D- How to instantiate a random object from an array?
//   Links: https://answers.unity.com/questions/915759/how-to-instantiate-a-random-object-from-an-array.html
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickEnemyBall : MonoBehaviour {

	[SerializeField] GameObject[] enemyBalls;
	int myIndex = -1;
	public float pickTimer;
	public float pickTimerLimit = 5.0f;

	// Use this for initialization
	void Start () {
		pickTimer = 3.0f;
		myIndex = 0;
	}

	public void ResetTimeSinceLastEnemy(){
		pickTimer = pickTimerLimit;
	}

	// Update is called once per frame
	void Update () {
		pickTimer -= Time.deltaTime;
		if(pickTimer <= 0.0f){
			if(myIndex >= 0)
				Instantiate (enemyBalls[myIndex]);
			
			myIndex++;

			if (myIndex >= enemyBalls.Length)
				myIndex = 0;
			//myIndex = Random.Range (0, enemyBalls.Length);
			//Instantiate (enemyBalls[Random.Range (0, enemyBalls.Length)]);
			ResetTimeSinceLastEnemy ();
		}
	}
}
