﻿/*       
// Developer Name: Jesse Rivera and Lucas LaMonica
//   Contribution:
//   Feature- The red ball will bounce downward in a specific pattern.
//   Start & End dates- October 30th- November 15th
//   References: -Vector3.MoveTowards (movement stops)
//               -Unity3D- Find a transform object with tag
//               -Unity3D- Freeze rigidbody position in script
//   Links: -https://forum.unity.com/threads/vector3-movetowards-movement-stops.286609/
//          -https://answers/unity.com/questions/479889/find-a-transform-object-with-tag.html
//          -https://answers/unity.cpm/questions/767872/freeze-rigidbody-position-in-script.html
//
*/
// Contribution Jesse Rivera: Start(11/4/18) End(11/9/18) Starting the script by adding a respawn, and made waypoints for the ball to follow.
// 
/*
Partially writen by Johnathon Hrifko
Setup the void on trigger enter for the lives and all the void GameOveDeath stuff which is mostly leaderboard setup which is not currently in use.
	Start and End (11/9/17)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JR_EnemyBall : MonoBehaviour {

	//public GameObject myEnemy;
	public Transform[] target;
	public GameObject redBall;
	public float speed;
	private bool bouncing = false;
	public Animator anim;
	private int LifeisBad;
	public GameObject GameOverDeathCanvas;
	public Transform respawnPosition;

	public GameObject player;

	private int current;
	public bool isMoving = true;

	Vector3 BouncePosition;

	public AudioClip jumpLand;
	public AudioClip hitPlayer;
	public AudioSource AS;
	public WinConditionLevel1 WC;

	// Use this for initialization
	void Start () {
		respawnPosition = GameObject.FindGameObjectWithTag ("Respawn").transform;
		Debug.Log ("GameOverDeathCanvas" + GameOverDeathCanvas);
		player = GameObject.FindGameObjectWithTag ("Player");
		WC = WinConditionLevel1.FindObjectOfType<WinConditionLevel1> ();
		//GameOverDeathCanvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (isMoving == true && transform.position != target [current].position) {
			//transform.position = Vector3.Lerp (transform.position, target [current].position, speed * Time.deltaTime);
			Vector3 pos = Vector3.MoveTowards (transform.position,target[current].position, speed*Time.deltaTime);
			if(bouncing == true && !anim.IsInTransition (0)){
				Debug.Log ("Bouncing");
				anim.SetTrigger ("WillBounce");
				bouncing = false;
			}

			//gameObject.GetComponent <SphereCollider>().enabled = false;
			//gameObject.GetComponent <MeshRenderer>().enabled = false;
			//GetComponent<Rigidbody> ().velocity = new Vector2 (GetComponent<Rigidbody> ().velocity.x, 0.0f);
			GetComponent<Rigidbody> ().MovePosition (pos);
			float dist = Vector3.Distance (pos, target [current].position);
			if(dist < 0.0001f){
				isMoving = false;
				StartCoroutine (OnBlock ());
				current++;
				//if(blockTimer >= blockTimerLimit){
					//isMoving = true;
					//blockTimer = blockTimerLimit;
				//}
			}
			//DestroyObject (myEnemy);
			//StartCoroutine (StayOnCube ());
		}
		else{
			//BouncePosition = transform.position + Vector3.up * 5.0f;

			//Debug.Log ("bounce position is: " + BouncePosition.ToString ());

			//current = (current + 1) % target.Length;


			Debug.Log ("bounce");
			//GetComponent<Rigidbody> ().velocity = new Vector2 (GetComponent<Rigidbody> ().velocity.x, 3.0f);
			//bouncing = true;
			//Debug.Log ("Entering coroutine");

		

		//RB.velocity = new Vector3 (RB.velocity.z, -5.0f);
		}
		if(WC.allTurned == true){
			gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		}
	}

	IEnumerator OnBlock(){
		AS.clip = jumpLand;
		AS.Play ();
		yield return new WaitForSeconds (0.3f);
		isMoving = true;
		bouncing = true;
		yield return null;
	}

	void OnTriggerEnter(Collider other){

		if (other.tag == "Player"){

			LifeisBad = PlayerPrefs.GetInt("Lives");
			LifeisBad--;
			PlayerPrefs.SetInt("Lives", LifeisBad);

			if (PlayerPrefs.GetInt("Lives") == 0)
			{
				//sends you to void GameOveDeath when you have 0 lives left.
				GameOverDeath();

			}

			else
			{
				StartCoroutine (AfterCollision ());
				//if you have more than 0 lives left then reset the player position.


			}
		}

	}

	IEnumerator AfterCollision(){
		AS.clip = hitPlayer;
		AS.Play ();
		player.GetComponent <JR_Movement> ().enabled = false;
		gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		yield return new WaitForSeconds (0.5f);
		player.transform.position = respawnPosition.transform.position;
		player.GetComponent <JR_Movement> ().enabled = true;
		Destroy (redBall);
		StopCoroutine (AfterCollision ());
	}

	void GameOverDeath()
	{
		Time.timeScale = 0;
        //turns on the Game Over canvas when GameOverDeath(); is called.
        GameOverDeathCanvas.SetActive(true);
		//start of leader Board stuff
		if (PlayerPrefs.GetInt("Leader1") < PlayerPrefs.GetInt("FinalScore")){

			PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
			if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

			}

			if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

			}

			if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

			}

			if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

			}

			if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

			}

			if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

			}

			if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

			}

			if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("Leader1"));

			}

			if (PlayerPrefs.GetInt("Leader2") == PlayerPrefs.GetInt("Leader1")){

				PlayerPrefs.SetInt("Leader1", PlayerPrefs.GetInt("FinalScore"));

			}
		}

		if (PlayerPrefs.GetInt("Leader2") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader1")){

			PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
			if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

			}

			if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

			}

			if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

			}

			if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

			}

			if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

			}

			if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

			}

			if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("Leader2"));

			}

			if (PlayerPrefs.GetInt("Leader3") == PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader2", PlayerPrefs.GetInt("FinalScore"));

			}

			if (PlayerPrefs.GetInt("Leader3") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader2")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

				}

				if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

					PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("Leader3"));

				}

				if (PlayerPrefs.GetInt("Leader4") == PlayerPrefs.GetInt("Leader3")){

					PlayerPrefs.SetInt("Leader3", PlayerPrefs.GetInt("FinalScore"));

				}

			}
			if (PlayerPrefs.GetInt("Leader4") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader3")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("Leader4"));

				}

				if (PlayerPrefs.GetInt("Leader5") == PlayerPrefs.GetInt("Leader4")){

					PlayerPrefs.SetInt("Leader4", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader5") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader4")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("Leader5"));

				}

				if (PlayerPrefs.GetInt("Leader6") == PlayerPrefs.GetInt("Leader5")){

					PlayerPrefs.SetInt("Leader5", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader6") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader5")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("Leader6"));

				}

				if (PlayerPrefs.GetInt("Leader7") == PlayerPrefs.GetInt("Leader6")){

					PlayerPrefs.SetInt("Leader6", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader7") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader6")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("Leader7"));

				}

				if (PlayerPrefs.GetInt("Leader8") == PlayerPrefs.GetInt("Leader7")){

					PlayerPrefs.SetInt("Leader7", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader8") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader7")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("Leader8"));

				}

				if (PlayerPrefs.GetInt("Leader9") == PlayerPrefs.GetInt("Leader8")){

					PlayerPrefs.SetInt("Leader8", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader9") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader8")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("Leader9"));
				if (PlayerPrefs.GetInt("Leader10") == PlayerPrefs.GetInt("Leader9")){

					PlayerPrefs.SetInt("Leader9", PlayerPrefs.GetInt("FinalScore"));

				}

			}

			if (PlayerPrefs.GetInt("Leader10") < PlayerPrefs.GetInt("FinalScore") && PlayerPrefs.GetInt("FinalScore") < PlayerPrefs.GetInt("Leader9")){

				PlayerPrefs.SetInt("Leader10", PlayerPrefs.GetInt("FinalScore"));

			}
			//End of Leader Board Stuff}
		}
	}


}
