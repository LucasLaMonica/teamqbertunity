﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreStuff : MonoBehaviour {

    // Code by Henry Johnson
    //Text item that holds score should be a GUIText object (empty gameobject with GUIText component on it)
    public Text scoreText;

    //Int that holds the score value, changes based on input from outside sources.
    private int score;

    //Int that holds a target score that will increase lives whenever a point threshold is crossed.
    int targetScore = 500;

    // Sets int to how many lives are remaining
    private int Livesleft;



    void Start () {
        //Sets base score
            score = PlayerPrefs.GetInt("FinalScore");
        Livesleft = PlayerPrefs.GetInt("Lives");
        UpdateScore();
    }

    private void FixedUpdate()
    {
        if(score >= targetScore)
        {
            Livesleft = PlayerPrefs.GetInt("Lives");
            Livesleft++;
            PlayerPrefs.SetInt("Lives", Livesleft);
            targetScore += 500;
        }
    }

    //The way score is added, usually called by whatever is adding the score (such as changing colors, for example)
    public void AddScore(int newScoreValue)
    {
        score = newScoreValue + score;
        UpdateScore();

    }

    //Code that updates the score text to say what the score is. NOTE: DELETES EVERYTHING IN THE ACTUAL TEXT BOX AND REPLACES IT WITH THE BELLOW
    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void SetFinalScore()
    {
        PlayerPrefs.SetInt("FinalScore", score);
    }
}
