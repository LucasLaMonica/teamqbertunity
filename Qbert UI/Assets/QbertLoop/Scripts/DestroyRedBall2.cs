﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRedBall2 : MonoBehaviour {

	public GameObject enemyObjects;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "RedBall"){
			Debug.Log ("Fallen off the stage.");
			Destroy (enemyObjects);
		}
	}
}

