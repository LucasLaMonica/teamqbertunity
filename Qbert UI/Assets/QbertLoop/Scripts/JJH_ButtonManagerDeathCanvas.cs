﻿/*
Writen by Johnathon Hrifko
Set the Buttons for the Game Over Screen to do all of their proper things.
Start and End (11/6/18)
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JJH_ButtonManagerDeathCanvas : MonoBehaviour {

	public void RestartBtn(string Restart)
	{

		SceneManager.LoadScene(Restart);
        PlayerPrefs.SetInt("Lives", 3);

	}

	public void MainMenuBtn(string MainMenu)
	{

		SceneManager.LoadScene(MainMenu);

	}

	public void ExitGameBtn()
	{

		Application.Quit();

	}
}
