﻿// Contribution Jesse Rivera and Lucas LaMonica: Start(11/3/17) End (11/10/17) Made the basic movement for the player.
/*
Partially writen by Johnathon Hrifko
Setup for Pause Menu.  Used to start the pause menu and stop input from the player while also stoping the enemies.
	Start and End (11/7/17-11/15/17)
	*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JR_Movement : MonoBehaviour {
	
	public bool jumping = false;
	Rigidbody RB;
	public Transform DiscDestroy;

    /*This is for John's stuff*/
    public GameObject PauseMenu;
    public GameObject GameOverDeathCanvas;
    /*This is for John's stuff*/

	public AudioClip jumpLand;
	public AudioClip hitSlick;
	public AudioSource AS;

    // Use this for initialization
    void Start () {
		//Sets pause menu to not active
		PauseMenu.SetActive(false);
		RB = GetComponent <Rigidbody> ();
		Physics.IgnoreCollision (DiscDestroy.GetComponent <Collider>(), GetComponent <Collider>());
	}

	// Update is called once per frame
	void Update () {
		gameObject.GetComponent <AudioSource>().enabled = true;
        if (PauseMenu.activeInHierarchy == false)
        {

            Time.timeScale = 1;

        }

		//currentLocation = gameObject.transform.position;


		if(jumping == false){
			/*John added this*/
			if (PauseMenu.activeInHierarchy == false && GameOverDeathCanvas.activeInHierarchy == false)/*John added this*/
			{
				if (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown (KeyCode.UpArrow)){
					transform.eulerAngles = new Vector3 (0, 180, 0);
					RB.AddForce (-0.80632f * gameObject.transform.right + new Vector3 (0, 7, 0), ForceMode.VelocityChange);
					jumping = true;

				}

				if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown (KeyCode.RightArrow)){
					transform.eulerAngles = new Vector3 (0, -90, 0);
					RB.AddForce (-0.84733f * gameObject.transform.right + new Vector3 (0, 5, 0), ForceMode.VelocityChange);
					jumping = true;
				}

				if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown (KeyCode.LeftArrow)){
					transform.eulerAngles = new Vector3 (0, 90, 0);
					RB.AddForce (-0.80633f * gameObject.transform.right + new Vector3 (0, 7, 0), ForceMode.VelocityChange);
					jumping = true;
				}


				if (Input.GetKeyDown (KeyCode.S) || Input.GetKeyDown (KeyCode.DownArrow)) {
					transform.eulerAngles = new Vector3 (0, 0, 0);
					RB.AddForce (-0.84731f * gameObject.transform.right + new Vector3 (0, 5, 0), ForceMode.VelocityChange);
					jumping = true;
				}


				if (Input.GetKeyDown(KeyCode.R))
					SceneManager.LoadScene("QBERT");
			}



			//John aka JJH started edited here
			if (PauseMenu.activeInHierarchy == false && Input.GetKeyDown(KeyCode.Escape) && GameOverDeathCanvas.activeInHierarchy == false)
			{
				PauseMenu.SetActive(true);
				Time.timeScale = 0;
			}
			if (GameOverDeathCanvas.activeInHierarchy == true)
			{

				Time.timeScale = 0;

			}

		}
		}
	private void OnCollisionEnter (Collision other){
		if(other.gameObject.tag == "Cube"){
			jumping = false;
			AS.clip = jumpLand;
			AS.Play ();
		}
		if(other.gameObject.tag == "Slick"){
			AS.clip = hitSlick;
			AS.Play ();
		}
	}
        


	//John finished editing here


}
