﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Developer Name: Alanis Arriaga and Lucas LaMonica
Contribution:
Destroy disc prefab when the play jumps onto the disc it'll brin them to the top of the map causing the disc to destroy itself and then drop the player onto the first block.
Start Date: 11/3/2017
End Date: 11/15/2017
//*/






public class DestroyDisc : MonoBehaviour {

    public float Timer;
    public float TimerLimit = 6.0f;
    public float Timepassed;
    public GameObject disc; 

	public AudioClip deadDisc;
	public AudioSource AS;

	// Use this for initialization


	void Start ()
    { 
        Timer = TimerLimit;
        Timepassed = 0.0f; 
	}
	

    void OnTriggerEnter(Collider Other)
    {

        if(Other.tag == "disc")
        {
            Debug.Log("Destroy"); 
			AS.clip = deadDisc;
			AS.Play ();

            Timepassed += Time.deltaTime;
            if (Timepassed >= 0.0f)
            {
                Destroy(disc); 
            }

            FindObjectOfType<JR_Movement>().gameObject.transform.parent = Other.transform.parent;
            FindObjectOfType<JR_Movement>().GetComponent<Rigidbody>().isKinematic = false;
            FindObjectOfType<JR_Movement>().enabled = true;
			FindObjectOfType<JR_Movement> ().GetComponent <AudioSource>().enabled = true;
            transform.parent = null;
            Debug.Log("Detached"); 

        }

    }

   
    // Update is called once per frame
    void Update ()
    {

        Timer += Time.deltaTime; 
        if (Timer <= 0.0f)
        {
            Timer = (10);
        }

    }
}
