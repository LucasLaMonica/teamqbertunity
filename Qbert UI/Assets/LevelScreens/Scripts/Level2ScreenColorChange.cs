﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- After a certain amount of time, these cubes will have the ability to change colors. When QBert touches an cube, that cube will change a specific color. The same will happen when QBert jumps on the same cube for a second time, but not the third time. This will only work in Level 2.
//   Start & End dates- November 5th-15th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2ScreenColorChange : MonoBehaviour {

	public bool isCyan = true;
	public bool isYellow = false;
	public bool isGreen = false;

	public float colorTimer;
	public float colorTimerLimit = 2.0f;
	public bool timeUp;

	public bool topBlock;

	public AudioClip jumpLand;
	public AudioSource AS;

	// Use this for initialization
	void Start () {
		colorTimer = colorTimerLimit;
		timeUp = false;
		//Sets starting color and time limit
		gameObject.GetComponent <Renderer>().material.color = Color.cyan;
	}

	// Update is called once per frame
	void Update () {
		colorTimer -= Time.deltaTime;
		if (colorTimer <= 0.0f){
			timeUp = true;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.collider.tag == "Player"){
			AS.clip = jumpLand;
			AS.Play ();
			if(gameObject.GetComponent <Renderer>().material.color == Color.cyan && timeUp == true && isCyan == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.yellow;
				StartCoroutine (WaitForChange());
			}
			if(gameObject.GetComponent <Renderer>().material.color == Color.yellow && timeUp == true && isYellow == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.green;
				isYellow = false;
				isGreen = true;
			}
		}
	}

	void OnCollisionExit(Collision other){
		if(other.collider.tag == "Player" && topBlock == true){
			topBlock = false;
		}
	}

	IEnumerator WaitForChange(){
		yield return new WaitForSeconds(0.5f);
		isCyan = false;
		isYellow = true;
		StopCoroutine (WaitForChange ());
	}
}
