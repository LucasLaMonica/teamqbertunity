﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- Two timers will play. The first has the level number flashing back and forth. Once the timer is done, another one will start as an animation plays. When this second timer runs out the level will load.
//   Start & End dates- November 6th-10th
//   References: Unity 3D- Unity 2D: perform action after animation is done.
//   Links: https://answers.unity.cpm/questions/675028/unity-2d-perform-action-after-animation-is-done.html
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationTimer : MonoBehaviour {

	public float numberTimer;
	public float numberTimerLimit = 2.0f;
	public GameObject levelNumber;
	public bool isShowing;

	public Animator anim;
	public float animTimer;
	public float animTimerLimit = 4.0f;

	public string LoadLevel;

	// Use this for initialization
	void Start () {
		numberTimer = numberTimerLimit;
		animTimer = animTimerLimit;
		levelNumber.SetActive (true);
		isShowing = true;
	}

	// Update is called once per frame
	void Update () {
		if(isShowing == true){
			StartCoroutine (NumberFlash ());
		}
	}

	IEnumerator NumberFlash(){
		numberTimer -= Time.deltaTime;
		if(numberTimer > 0.0f){
			if(levelNumber.activeInHierarchy == true){
				yield return new WaitForSeconds (0.45f);
				levelNumber.SetActive (false);
			}
			if(levelNumber.activeInHierarchy == false){
				yield return new WaitForSeconds (0.45f);
				levelNumber.SetActive (true); 
			}
		}
		if(numberTimer <= 0.0f){
			numberTimer = (0);
			Debug.Log ("Time's up");
			AnimationPlay ();
			//SceneManager.LoadScene (LoadLevel);
		}
	}

	void AnimationPlay(){
		animTimer -= Time.deltaTime;
		if(animTimer > 0.0f){
			anim.SetTrigger ("JumpBegin");
		}
		if(animTimer <= 0.0f){
			SceneManager.LoadScene (LoadLevel);
		}
	}
}
