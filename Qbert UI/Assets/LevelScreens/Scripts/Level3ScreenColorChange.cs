﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature-fter a certain amount of time, these cubes will have the ability to change colors. When QBert touches an cube, the cube's color will altercate back and forth between two colors. This will only work in Level 3.
//   Start & End dates- November 5th-15th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3ScreenColorChange : MonoBehaviour {

	public bool isCyan = true;
	public bool isBlue = false;

	public float colorTimer;
	public float colorTimerLimit = 2.0f;
	public bool timeUp;

	public bool topBlock;

	public AudioClip jumpLand;
	public AudioSource AS;

	// Use this for initialization
	void Start () {
		colorTimer = colorTimerLimit;
		timeUp = false;
		//Sets starting color and time limit
		gameObject.GetComponent <Renderer>().material.color = Color.cyan;
	}

	// Update is called once per frame
	void Update () {
		colorTimer -= Time.deltaTime;
		if (colorTimer <= 0.0f){
			timeUp = true;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.collider.tag == "Player"){
			AS.clip = jumpLand;
			AS.Play ();
			if(gameObject.GetComponent <Renderer>().material.color == Color.cyan && timeUp == true && isCyan == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.blue;
				StartCoroutine (WaitForChangeCyan());
			}
			if (gameObject.GetComponent <Renderer>().material.color == Color.blue && timeUp == true && isBlue == true && topBlock == false){
				gameObject.GetComponent <Renderer>().material.color = Color.cyan;
				StartCoroutine (WaitForChangeBlue ());
			}
		}
	}

	void OnCollisionExit(Collision other){
		if(other.collider.tag == "Player" && topBlock == true){
			topBlock = false;
		}
	}

	IEnumerator WaitForChangeCyan(){
		yield return new WaitForSeconds(0.5f);
		isCyan = false;
		isBlue = true;
		StopCoroutine (WaitForChangeCyan ());
	}

	IEnumerator WaitForChangeBlue(){
		yield return new WaitForSeconds(0.5f);
		isCyan = true;
		isBlue = false;
		StopCoroutine (WaitForChangeBlue ());
	}
}
