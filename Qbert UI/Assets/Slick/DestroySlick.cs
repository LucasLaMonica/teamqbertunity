﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- When a Slick falls into the trigger, the specific Slick AI will be destroyed.
//   Start & End dates- November 8th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySlick : MonoBehaviour {

	public GameObject enemyObjects;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Slick"){
			Debug.Log ("Fallen off the stage.");
			Destroy (enemyObjects);
		}
	}
}