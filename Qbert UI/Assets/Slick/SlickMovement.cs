﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- The Slick enemy will bounce in a specific pattern
//   Start & End dates- November 7th-15th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlickMovement : MonoBehaviour {

	public GameObject myEnemy;
	public GameObject Slick;
	public Transform[] target;
	public float speed;
	public bool bouncing = false;
	public Animator anim;

	public GameObject player;

	private int current;
	public bool isMoving = true;

	Vector3 BouncePosition;

	public AudioClip jumpLand;
	public AudioSource AS;
	private WinConditionLevel1 WC;

	// Use this for initialization
	void Start () {
		WC = WinConditionLevel1.FindObjectOfType<WinConditionLevel1> ();
	}

	// Update is called once per frame
	void Update () 
	{

		if (isMoving == true && transform.position != target [current].position) {
			//transform.position = Vector3.Lerp (transform.position, target [current].position, speed * Time.deltaTime);
			Vector3 pos = Vector3.MoveTowards (transform.position,target[current].position, speed*Time.deltaTime);
			player = GameObject.FindGameObjectWithTag ("Player");
			if(bouncing == true && !anim.IsInTransition (0)){
				anim.SetTrigger ("WillBounce");
				bouncing = false;
			}
				
			GetComponent<Rigidbody> ().MovePosition (pos);
			float dist = Vector3.Distance (pos, target [current].position);
			if(dist < 0.0001f){
				isMoving = false;
				StartCoroutine (OnBlock ());
				current++;
			}

		}
		else{

		}

		if(WC.allTurned == true){
			gameObject.GetComponent <Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.collider.tag == "Player"){
			Destroy (Slick);
		}
	}

	IEnumerator OnBlock(){
		AS.clip = jumpLand;
		AS.Play ();
		yield return new WaitForSeconds (0.3f);
		isMoving = true;
		bouncing = true;
		yield return null;
	}

}
