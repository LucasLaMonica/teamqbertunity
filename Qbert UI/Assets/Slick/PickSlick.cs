﻿/*       
// Developer Name: Lucas LaMonica
//   Contribution:
//   Feature- After a certain increment of time, the script will cycle through an array and spawn one of the Slick AIs.
//   Start & End dates- November 8th
//   References:
//   Links:
//
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickSlick : MonoBehaviour {

	[SerializeField] GameObject[] enemySlick;
	int myIndex = -1;
	public float pickTimer;
	public float pickTimerLimit = 15.0f;

	// Use this for initialization
	void Start () {
		pickTimer = 15.0f;
		myIndex = 0;
	}

	public void ResetTimeSinceLastEnemy(){
		pickTimer = pickTimerLimit;
	}

	// Update is called once per frame
	void Update () {
		pickTimer -= Time.deltaTime;
		if(pickTimer <= 0.0f){
			if(myIndex >= 0)
				Instantiate (enemySlick[myIndex]);

			myIndex++;

			if (myIndex >= enemySlick.Length)
				myIndex = 0;
			//myIndex = Random.Range (0, enemyBalls.Length);
			//Instantiate (enemyBalls[Random.Range (0, enemyBalls.Length)]);
			ResetTimeSinceLastEnemy ();
		}
	}
}

